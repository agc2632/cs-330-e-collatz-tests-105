#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve, collatz_doer

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_2(self):
        s = "72850 308\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  72850)
        self.assertEqual(j, 308)

    def test_read_3(self):
        s = "28765 85696\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 28765)
        self.assertEqual(j, 85696)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    # check reverse order
    def test_eval_5(self):
        v = collatz_eval(1000, 900)
        self.assertEqual(v, 174)

    # check negative
    def test_eval_6(self):
        v = collatz_eval(-1, 5)
        self.assertEqual(v, "Invalid Input")

    # check zero
    def test_eval_7(self):
        v = collatz_eval(0, 30)
        self.assertEqual(v, "Invalid Input")

    # check float
    def test_eval_8(self):
        v = collatz_eval(1.34, 182)
        self.assertEqual(v, "Invalid Input")

    # ----
    # doer
    # ----

    # check lowest odd
    def test_doer_1(self):
        x = collatz_doer(1,1)
        self.assertEqual(x,1)

    # check zero
    def test_doer_2(self):
        x = collatz_doer(0,1)
        self.assertEqual(x,"Invalid Input")

    # check negative
    def test_doer_3(self):
        x = collatz_doer(-5,1)
        self.assertEqual(x,"Invalid Input")

    # check float
    def test_doer_4(self):
        x = collatz_doer(10.5,1)
        self.assertEqual(x,"Invalid Input")
        
    # check highest odd
    def test_doer_5(self):
        x = collatz_doer(999999,1)
        self.assertEqual(x,259)
    
    # check highest even
    def test_doer_6(self):
        x = collatz_doer(999998,1)
        self.assertEqual(x,259)
    
    # check lowest even
    def test_doer_7(self):
        x = collatz_doer(2,1)
        self.assertEqual(x,2)  





    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 11082, 99999, 12345)
        self.assertEqual(w.getvalue(), "11082 99999 12345\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 44119, 87626, 999)
        self.assertEqual(w.getvalue(), "44119 87626 999\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_2(self):
        r = StringIO("30886 1508\n9 419\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "30886 1508 308\n9 419 144\n")

    def test_solve_3(self):
        r = StringIO("999900 999999\n1 1\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "999900 999999 259\n1 1 1\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
