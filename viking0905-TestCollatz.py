#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read1(self):
        s = "-5 -2\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  -5)
        self.assertEqual(j, -2)

    def test_read2(self):
        s = "59 237\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  59)
        self.assertEqual(j, 237)

    def test_read3(self):
        s = "19 19\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  19)
        self.assertEqual(j, 19)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(1000, 900)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(100, 100)
        self.assertEqual(v, 26)

    def test_eval_6(self):
        v = collatz_eval(-3, -1)
        self.assertEqual(v, "Invalid input: out of range")

    def test_eval_7(self):
        v = collatz_eval(9, 1000000)
        self.assertEqual(v, "Invalid input: out of range")

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print1(self):
        w = StringIO()
        collatz_print(w, -3, -1, "Invalid input: out of range")
        self.assertEqual(w.getvalue(), "-3 -1 Invalid input: out of range\n")

    def test_print2(self):
        w = StringIO()
        collatz_print(w, 100, 100, 26)
        self.assertEqual(w.getvalue(), "100 100 26\n")

    def test_print3(self):
        w = StringIO()
        collatz_print(w, 900, 1000, 174)
        self.assertEqual(w.getvalue(), "900 1000 174\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve1(self):
        r = StringIO("-3 -1\n 100 200\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "-3 -1 Invalid input: out of range\n100 200 125\n")

    def test_solve2(self):
        r = StringIO("100 100\n 1 10\n 3\n1 10\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "100 100 26\n1 10 20\n3 _ Invalid input: please provide two integers\n1 10 20\n")

    def test_solve3(self):
        r = StringIO("1 1000000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 1000000 Invalid input: out of range\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
